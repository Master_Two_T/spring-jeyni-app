package org.jeyni.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

public abstract class Helper {

	private static final Logger logger = Logger.getLogger(Helper.class);

	private Helper() {
	}

	public static Date convertToSQLDate(java.util.Date date) {
		if (date == null) {
			return null;
		}
		Long time = date.getTime();
		return new Date(time);
	}

	public static String convertDateToString(Date date, String pattern) {
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}

	public static java.util.Date convertStringToDate(String date, String pattern) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		java.util.Date dateToReturn = null;
		try {
			dateToReturn = formatter.parse(date);
		} catch (ParseException e) {
			logger.error(e);
			return null;
		}
		return dateToReturn;
	}

	public static Date convertStringToSQLDate(String date, String pattern) {
		return convertToSQLDate(convertStringToDate(date, pattern));
	}

	public static File convertMultiPartFileToFile(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		try {
			if (convFile.createNewFile()) {
				try (FileOutputStream fos = new FileOutputStream(convFile)) {
					fos.write(file.getBytes());
				}
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return convFile;
	}

	public static String getExtension(MultipartFile file) {
		logger.debug("file name: " + file.getOriginalFilename());
		return file.getOriginalFilename().split("\\.")[file.getOriginalFilename().split("\\.").length - 1];
	}
}
