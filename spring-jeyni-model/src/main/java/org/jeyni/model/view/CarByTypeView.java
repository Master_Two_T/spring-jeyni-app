package org.jeyni.model.view;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarByTypeView {
	
	private String modelType;
	
	private BigDecimal totalCostPrice;
	
	private BigDecimal totalSellPrice;
	
	private Integer production;
	
	private BigDecimal singleCostPrice;
	
	private BigDecimal singleSellPrice;

}
