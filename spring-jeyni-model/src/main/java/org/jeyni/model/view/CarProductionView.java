package org.jeyni.model.view;

import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class CarProductionView {

	private String date;

	@NonNull
	private String uuid;

	private HashMap<String, Integer> typeProductionMap = new HashMap<>();

	private Integer totalProduction;
	
	public CarProductionView(String uuid) {
		this.uuid = uuid;
	}
	
}
