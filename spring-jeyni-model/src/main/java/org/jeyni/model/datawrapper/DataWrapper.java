package org.jeyni.model.datawrapper;

import java.util.ArrayList;
import java.util.List;

import org.jeyni.model.entity.Car;
import org.jeyni.model.entity.CarProduction;

import lombok.Getter;

public class DataWrapper {

	@Getter
	private List<Car> carList;

	@Getter
	private List<CarProduction> carProductionList;

	public DataWrapper() {
		this.carList = new ArrayList<>();
		this.carProductionList = new ArrayList<>();
	}

}
