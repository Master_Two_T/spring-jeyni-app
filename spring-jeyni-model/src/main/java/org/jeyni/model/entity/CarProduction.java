package org.jeyni.model.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class CarProduction {

	@Getter
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Getter
	private String uuid;

	@Getter
	@Setter
	@ElementCollection(fetch = FetchType.EAGER)
	private List<Car> carList;

	@Getter
	@Setter
	private Date date;

	public CarProduction() {
		this.uuid = UUID.randomUUID().toString();
		this.carList = new ArrayList<>();
	}

	public CarProduction(Date date, List<Car> carList) {
		this.carList = carList;
		this.date = date;
		this.uuid = UUID.randomUUID().toString();
	}
}
