package org.jeyni.model.entity;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Car {

	@Getter
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Getter
	private String uuid;

	@Getter
	@Setter
	private BigDecimal sellPrice;

	@Getter
	@Setter
	private BigDecimal productionPrice;

	@Getter
	@Setter
	private TYPE modelType;

	public Car() {
		this.uuid = UUID.randomUUID().toString();
	}

	public Car(double sellPrice, double productionPrice, TYPE modelType) {
		this.sellPrice = BigDecimal.valueOf(sellPrice);
		this.productionPrice = BigDecimal.valueOf(productionPrice);
		this.modelType = modelType;
		this.uuid = UUID.randomUUID().toString();
	}

}
