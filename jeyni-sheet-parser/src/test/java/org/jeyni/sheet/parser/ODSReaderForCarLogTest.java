package org.jeyni.sheet.parser;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.List;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ch.qos.logback.classic.Level;

@RunWith(MockitoJUnitRunner.class)
public class ODSReaderForCarLogTest {

	private static final String FILE_NAME = "carFile.ods";
	private static final String PATH = "./src/test/resources/";

	@Mock
	private Appender mockAppender;

	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

	@Before
	public void setUp() {
		Logger root = Logger.getRootLogger();
		root.addAppender(mockAppender);
		when(mockAppender.getName()).thenReturn("MOCK");
		root.addAppender(mockAppender);

		File file = new File(PATH + FILE_NAME);
		ODSReaderForCarProduction.readODSFile(file);
	}

	@Test
	public void loggerTestEventsTriggered() {
		verify(mockAppender, atLeast(1)).doAppend(captorLoggingEvent.capture());
		List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
		assertThat(loggingEvents.get(0).getLevel().toString(), equalTo(Level.INFO.toString()));
		assertThat(loggingEvents.get(0).getMessage().toString(), equalTo("number of sheets: 2"));
	}
}
