package org.jeyni.sheet.parser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.jeyni.model.datawrapper.DataWrapper;
import org.jeyni.model.entity.Car;
import org.jeyni.model.entity.CarProduction;
import org.jeyni.model.entity.TYPE;
import org.jeyni.utilities.Helper;
import org.junit.BeforeClass;
import org.junit.Test;

public class ODSReaderForCarProductionTest {

	private static final String FILE_NAME = "carFile.ods";
	private static final String PATH = "./src/test/resources/";
	private static DataWrapper dataWrapper = null;

	@BeforeClass
	public static void setUp() {
		File file = new File(PATH + FILE_NAME);
		dataWrapper = ODSReaderForCarProduction.readODSFile(file);
	}

	@Test
	public void carAttrbiuteTest() {
		Car car = dataWrapper.getCarList().get(0);
		assertEquals(TYPE.A, car.getModelType());
	}

	@Test
	public void productionDateTest() {
		List<CarProduction> carProductionList = dataWrapper.getCarProductionList();
		List<CarProduction> filteredList = carProductionList.stream().filter( //
				carProduction -> Helper.convertDateToString(carProduction.getDate(), "dd/MM/yyyy").equals("01/01/2018")) //
				.collect(Collectors.toList()); //
		assertEquals(1, filteredList.size());
	}

}
