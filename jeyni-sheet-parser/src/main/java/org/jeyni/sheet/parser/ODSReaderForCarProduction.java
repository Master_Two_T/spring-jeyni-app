package org.jeyni.sheet.parser;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jeyni.model.datawrapper.DataWrapper;
import org.jeyni.model.entity.Car;
import org.jeyni.model.entity.CarProduction;
import org.jeyni.model.entity.TYPE;
import org.jeyni.utilities.Helper;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class ODSReaderForCarProduction {

	private static final Logger logger = Logger.getLogger(ODSReaderForCarProduction.class);

	// Sheet page 1
	private static final int CAR_TYPE_COL_NUMBER = 0;
	private static final int CAR_SELL_PRICE_COL_NUMBER = 1;
	private static final int CAR_COST_COL_NUMBER = 2;

	// Sheet page 2
	private static final int PRODUCTION_TYPE_COL_NUMBER = 2;
	private static final int PRODUCTION_PRODUCTION_COL_NUMBER = 1;
	private static final int PRODUCTION_DATE_COL_NUMBER = 0;

	private static HashMap<java.sql.Date, HashMap<String, Integer>> carProductionDateCarProductionTypeMap = new HashMap<>();
	private static EnumMap<TYPE, HashMap<Double, Double>> carTypePriceCostMap = new EnumMap<TYPE, HashMap<Double, Double>>(TYPE.class);

	private ODSReaderForCarProduction() {

	}

	public static DataWrapper readODSFile(File file) {
		Sheet sheet;
		DataWrapper dataWrapper = new DataWrapper();
		try {
			SpreadSheet spreadSheet = SpreadSheet.createFromFile(file);
			int sheetCount = spreadSheet.getSheetCount();
			logger.info("number of sheets: " + sheetCount);
			for (int sheetIndex = 0; sheetIndex < sheetCount; sheetIndex++) {
				if (logger.isDebugEnabled()) {
					logger.debug("Sheet: " + sheetIndex);
				}
				// Getting the current sheet for manipulation pass sheet name as string
				sheet = spreadSheet.getSheet(sheetIndex);
				// Get row count and column count
				int nColCount = sheet.getColumnCount();
				int nRowCount = sheet.getRowCount();
				if (logger.isDebugEnabled()) {
					logger.debug("Rows: " + nRowCount);
					logger.debug("Cols: " + nColCount);
				}
				if (sheetIndex == 0) {
					parseCarTemplateTable(sheet, nColCount, nRowCount, dataWrapper.getCarList());
				} else if (sheetIndex == 1) {
					parseCarProductionTable(sheet, nColCount, nRowCount, dataWrapper.getCarProductionList());
				}
			}
		} catch (IOException e) {
			logger.error(e);
			return dataWrapper;
		}
		return dataWrapper;
	}

	private static void parseCarProductionTable(Sheet sheet, int nColCount, int nRowCount, List<CarProduction> carProductionList) {
		for (int nRowIndex = 0; nRowIndex < nRowCount; nRowIndex++) {
			parseCurrentRowForCarProduction(sheet, nColCount, nRowIndex, carProductionDateCarProductionTypeMap);
		}
		Set<Entry<Date, HashMap<String, Integer>>> entrySetCarProductionDateCarProductionTypeMap = carProductionDateCarProductionTypeMap.entrySet();
		for (Entry<Date, HashMap<String, Integer>> entry : entrySetCarProductionDateCarProductionTypeMap) {
			Date date = entry.getKey();
			List<Car> carList = new ArrayList<>();
			Set<Entry<String, Integer>> entrySet = entry.getValue().entrySet();
			for (Entry<String, Integer> entry2 : entrySet) {
				TYPE type = TYPE.valueOf(entry2.getKey());
				Integer production = entry2.getValue();
				for (int i = 0; i < production; i++) {
					Set<Entry<Double, Double>> entrySetCarTypePriceCostMap = carTypePriceCostMap.get(type).entrySet();
					Double costPrice = null;
					Double sellPrice = null;
					for (Entry<Double, Double> entry3 : entrySetCarTypePriceCostMap) {
						sellPrice = entry3.getKey();
						costPrice = entry3.getValue();
					}
					carList.add(new Car(sellPrice, costPrice, type));
				}
			}
			carProductionList.add(new CarProduction(date, carList));
		}
	}

	private static void parseCurrentRowForCarProduction(Sheet sheet, int nColCount, int nRowIndex, HashMap<java.sql.Date, HashMap<String, Integer>> carProductionDateCarProductionTypeMap) {
		String type = null;
		Date date = null;
		Integer number = null;
		boolean isTitle = false;
		for (int nColIndex = 0; nColIndex < nColCount; nColIndex++) {
			MutableCell<SpreadSheet> cell = sheet.getCellAt(nColIndex, nRowIndex);
			if (nRowIndex == 0) {
				isTitle = true;
				continue;
			}
			logger.debug("nRowIndex: " + nRowIndex + " nColIndex: " + nColIndex + " value:" + cell.getTextValue());
			switch (nColIndex) {
			case PRODUCTION_DATE_COL_NUMBER:
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				try {
					date = Helper.convertToSQLDate(dateFormat.parse(cell.getTextValue()));
				} catch (ParseException e) {
					logger.error(e);
				}
				break;
			case PRODUCTION_PRODUCTION_COL_NUMBER:
				number = Integer.valueOf(cell.getTextValue());
				break;
			case PRODUCTION_TYPE_COL_NUMBER:
				type = cell.getTextValue();
				break;
			default:
				break;
			}
		}
		if (!isTitle && date != null && type != null) {
			HashMap<String, Integer> temp = new HashMap<>();
			temp.put(type, number);
			if (carProductionDateCarProductionTypeMap.get(date) == null || carProductionDateCarProductionTypeMap.get(date).size() == 0) {
				carProductionDateCarProductionTypeMap.put(date, temp);
			} else {
				carProductionDateCarProductionTypeMap.get(date).put(type, number);
			}
		}
	}

	private static void parseCarTemplateTable(Sheet sheet, int nColCount, int nRowCount, List<Car> cars) {
		for (int nRowIndex = 0; nRowIndex < nRowCount; nRowIndex++) {
			parseCurrentRowForCarTemplate(sheet, nColCount, cars, nRowIndex);
		}
	}

	private static void parseCurrentRowForCarTemplate(Sheet sheet, int nColCount, List<Car> cars, int nRowIndex) {
		TYPE type = null;
		Double sellPrice = null;
		Double costPrice = null;
		boolean isTitle = false;
		for (int nColIndex = 0; nColIndex < nColCount; nColIndex++) {
			MutableCell<SpreadSheet> cell = sheet.getCellAt(nColIndex, nRowIndex);
			if (nRowIndex == 0) {
				isTitle = true;
				continue;
			}
			logger.debug("nColIndex: " + nColIndex + " value:" + cell.getTextValue());
			switch (nColIndex) {
			case CAR_TYPE_COL_NUMBER:
				type = TYPE.valueOf(cell.getTextValue());
				break;
			case CAR_COST_COL_NUMBER:
				costPrice = Double.parseDouble(cell.getValue().toString());
				break;
			case CAR_SELL_PRICE_COL_NUMBER:
				sellPrice = Double.parseDouble(cell.getValue().toString());
				break;
			default:
				break;
			}
		}
		if (!isTitle && sellPrice != null && costPrice != null && type != null) {
			cars.add(new Car(sellPrice, costPrice, type));
			HashMap<Double, Double> temp = new HashMap<>();
			temp.put(sellPrice, costPrice);
			carTypePriceCostMap.put(type, temp);
		}
	}
}
