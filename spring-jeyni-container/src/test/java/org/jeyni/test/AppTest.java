package org.jeyni.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.jeyni.container.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class AppTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getCarsTest() throws Exception {
		this.mockMvc.perform(get("/api/cars")) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)); //
	}

	@Test
	public void getCarByIdTest() throws Exception {
		this.mockMvc.perform(get("/api/cars/10")).andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)) //
				.andExpect(jsonPath("modelType", equalTo("A"))); //
	}

	@Test
	public void getCarProductionsTest() throws Exception {
		this.mockMvc.perform(get("/api/productions")) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)) //
				.andExpect(jsonPath("$.length()", equalTo(15))) //
				.andExpect(jsonPath("$[0].date", equalTo("01/01/2018"))) //
				.andExpect(jsonPath("$[0].typeProductionMap.A", equalTo(10))) //
				.andExpect(jsonPath("$[0].typeProductionMap.B", equalTo(12))) //
				.andExpect(jsonPath("$[0].typeProductionMap.C", equalTo(8))) //
				.andExpect(jsonPath("$[0].totalProduction", equalTo(30))); //
	}

	@Test
	public void getCarsByType() throws Exception {
		this.mockMvc.perform(get("/api/cars/types")) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)) //
				.andExpect(jsonPath("$.length()", equalTo(3))); //
	}

	@Test
	public void getCarProductionBetweenDates() throws Exception {
		this.mockMvc.perform(get("/api/productions/dates") //
				.param("beginningDate", "01/01/2018") //
				.param("endDate", "04/01/2018")) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)) //
				.andExpect(jsonPath("$.length()", equalTo(4))); //
	}
	
	@Test
	public void getCarByTypeBetweenDates() throws Exception {
		this.mockMvc.perform(get("/api/cars/types/dates") //
				.param("beginningDate", "01/01/2018") //
				.param("endDate", "04/01/2018")) //
				.andExpect(status().isOk()) //
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)) //
				.andExpect(jsonPath("$.length()", equalTo(3))); //
	}
}
