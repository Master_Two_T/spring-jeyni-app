package org.jeyni.container;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"org.jeyni"})
@EntityScan(basePackages = {"org.jeyni"})
@ComponentScan(basePackages = {"org.jeyni"})
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
