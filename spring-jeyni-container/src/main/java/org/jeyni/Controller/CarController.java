package org.jeyni.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jeyni.business.contract.Business;
import org.jeyni.business.exception.BusinessException;
import org.jeyni.model.entity.Car;
import org.jeyni.model.view.CarByTypeView;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class CarController {

	private final Business business;

	private static final Logger logger = Logger.getLogger(CarController.class);

	public CarController(Business business) {
		this.business = business;
	}

	@GetMapping("/cars")
	@CrossOrigin
	public List<Car> getCars() {
		return this.business.findAll();
	}

	@GetMapping("/cars/types")
	@CrossOrigin
	public List<CarByTypeView> getCarsListByType() {
		return this.business.findCarsByType();
	}

	@GetMapping("/cars/types/dates")
	@CrossOrigin
	public List<CarByTypeView> getCarsListByTypeAndByDate(@RequestParam("beginningDate") String beginningDate, @RequestParam("endDate") String endDate) {
		try {
			return this.business.findCarsByTypeAndByDate(beginningDate, endDate);
		} catch (BusinessException e) {
			logger.error(e);
			return new ArrayList<>();
		}
	}

	@GetMapping("/cars/{id}")
	@CrossOrigin
	Car getCarById(@PathVariable long id) {
		return business.findById(id);
	}

	@PostMapping("/cars")
	@CrossOrigin
	public String postCar(@RequestBody Car car) {
		this.business.save(car);
		return String.valueOf(car.getId());
	}

}
