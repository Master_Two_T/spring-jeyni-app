package org.jeyni.controller;

import java.util.List;

import org.jeyni.business.contract.Business;
import org.jeyni.business.exception.BusinessException;
import org.jeyni.model.view.CarProductionView;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api")
public class CarProductionController {

	private final Business business;

	public CarProductionController(Business business) {
		this.business = business;
	}

	@GetMapping("/productions")
	@CrossOrigin
	public List<CarProductionView> getCarProductions() {
		return this.business.findAllCarProduction();
	}

	@PostMapping("/productions/file")
	@CrossOrigin
	public String postCarProductions(@RequestParam("file") MultipartFile file) {
		this.business.saveFromODSFile(file);
		return "{message: cars created}";
	}

	@GetMapping("/productions/dates")
	@CrossOrigin
	public List<CarProductionView> getCarProductionsBetwennDate(@RequestParam("beginningDate") String beginningDate, @RequestParam("endDate") String endDate) throws BusinessException {
		return this.business.findCarProducrionByDateRange(beginningDate, endDate);
	}
}
