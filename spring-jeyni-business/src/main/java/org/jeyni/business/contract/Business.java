package org.jeyni.business.contract;

import java.util.List;

import org.jeyni.business.exception.BusinessException;
import org.jeyni.model.entity.Car;
import org.jeyni.model.view.CarByTypeView;
import org.jeyni.model.view.CarProductionView;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface Business {

	public List<Car> findAll();

	public void save(Car car);

	public Car findByUuid(String uuid);

	public List<CarProductionView> saveFromODSFile(MultipartFile file);

	public List<CarProductionView> findAllCarProduction();

	public CarProductionView findCarProducrionByDate(String date);

	public List<CarProductionView> findCarProducrionByDateRange(String beginningDate, String endDate) throws BusinessException;

	public Car findById(long id);

	public List<CarByTypeView> findCarsByType();

	public List<CarByTypeView> findCarsByTypeAndByDate(String beginningDate, String endDate) throws BusinessException;

}
