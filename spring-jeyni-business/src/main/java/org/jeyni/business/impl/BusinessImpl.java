package org.jeyni.business.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.jeyni.business.contract.Business;
import org.jeyni.business.exception.BusinessException;
import org.jeyni.consumer.repository.CarProductionRepository;
import org.jeyni.consumer.repository.CarRepository;
import org.jeyni.model.datawrapper.DataWrapper;
import org.jeyni.model.entity.Car;
import org.jeyni.model.entity.CarProduction;
import org.jeyni.model.entity.TYPE;
import org.jeyni.model.view.CarByTypeView;
import org.jeyni.model.view.CarProductionView;
import org.jeyni.sheet.parser.ODSReaderForCarProduction;
import org.jeyni.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@Component
public class BusinessImpl implements Business {

	private static final String DD_MM_YYYY = "dd/MM/yyyy";

	private static final Logger logger = Logger.getLogger(BusinessImpl.class);

	@Autowired
	private CarRepository carRepository;
	@Autowired
	private CarProductionRepository carProductionRepository;

	@Override
	public List<Car> findAll() {
		return this.carRepository.findAll();
	}

	@Override
	public void save(Car car) {
		Car carToSave = new Car();
		TYPE modelType = car.getModelType();
		if (TYPE.A.equals(modelType) || TYPE.B.equals(modelType) || TYPE.C.equals(modelType)) {
			carToSave.setModelType(modelType);
		}
		carToSave.setProductionPrice(car.getProductionPrice());
		carToSave.setSellPrice(car.getSellPrice());
		this.carRepository.save(car);
	}

	@Override
	public Car findByUuid(String uuid) {
		return this.carRepository.findByUuid(uuid);
	}

	@Override
	public List<CarProductionView> saveFromODSFile(MultipartFile file) {
		logger.info("treating the file");
		DataWrapper dataWrapper = null;
		try {
			dataWrapper = parseFile(file);
		} catch (Exception e) {
			logger.error(e);
			return new ArrayList<>();
		}
		logger.info("file treated");
		List<CarProduction> productionList = dataWrapper.getCarProductionList();
		List<Car> cars = new ArrayList<>();
		List<CarProductionView> carProductionViewList = createCarProductionView(productionList, cars, false);
		this.carRepository.save(cars);
		this.carProductionRepository.save(productionList);
		return carProductionViewList;
	}

	@Override
	public Car findById(long id) {
		return this.carRepository.findOne(id);
	}

	@Override
	public List<CarProductionView> findAllCarProduction() {
		List<CarProduction> carProduction = this.carProductionRepository.findAllByOrderByDateAsc();
		return createCarProductionView(carProduction, true);
	}

	@Override
	public CarProductionView findCarProducrionByDate(String date) {
		CarProduction carProduction = this.carProductionRepository.findByDate(Helper.convertStringToSQLDate(date, DD_MM_YYYY));
		return fullFillCarProductionView(carProduction);
	}

	@Override
	public List<CarProductionView> findCarProducrionByDateRange(String beginningDate, String endDate) throws BusinessException {
		if (Helper.convertStringToDate(beginningDate, DD_MM_YYYY).after(Helper.convertStringToDate(endDate, DD_MM_YYYY))) {
			throw new BusinessException("start date should be before the end date");
		}
		List<CarProduction> carProductionList = this.carProductionRepository.findByDateBetweenOrderByDateAsc(Helper.convertStringToSQLDate(beginningDate, DD_MM_YYYY),
				Helper.convertStringToSQLDate(endDate, DD_MM_YYYY));
		if (carProductionList.isEmpty()) {
			throw new BusinessException("No production found for these date");
		} else {
			return createCarProductionView(carProductionList, true);
		}
	}

	@Override
	public List<CarByTypeView> findCarsByType() {
		return fullFillCarsTypeView(this.carRepository.findAll());
	}

	@Override
	public List<CarByTypeView> findCarsByTypeAndByDate(String beginningDate, String endDate) throws BusinessException {
		if (Helper.convertStringToDate(beginningDate, DD_MM_YYYY).after(Helper.convertStringToDate(endDate, DD_MM_YYYY))) {
			throw new BusinessException("start date should be before the end date");
		}
		List<CarProduction> carProductionList = this.carProductionRepository.findByDateBetweenOrderByDateAsc(Helper.convertStringToSQLDate(beginningDate, DD_MM_YYYY),
				Helper.convertStringToSQLDate(endDate, DD_MM_YYYY));
		List<Car> carRetrieved = new ArrayList<>();
		for (CarProduction carProduction : carProductionList) {
			carRetrieved.addAll(carProduction.getCarList());
		}
		if (carRetrieved.isEmpty()) {
			throw new BusinessException("No production found for these date");
		} else {
			return fullFillCarsTypeView(carRetrieved);
		}
	}

	private DataWrapper parseFile(MultipartFile file) throws BusinessException {
		String extension = Helper.getExtension(file);
		DataWrapper dataWrapper = null;
		switch (extension) {
		case "ods":
			dataWrapper = ODSReaderForCarProduction.readODSFile(Helper.convertMultiPartFileToFile(file));
			break;
		case "xls":
		case "xlsx":
			throw new BusinessException("Excel format will be implemented later");
		case "csv":
			throw new BusinessException("CSV format will be implemented later");
		default:
			throw new BusinessException("Unsuported format");
		}
		return dataWrapper;
	}

	private CarProductionView fullFillCarProductionView(CarProduction carProduction) {
		CarProductionView carProductionView = new CarProductionView(carProduction.getUuid());
		carProductionView.setDate(Helper.convertDateToString(carProduction.getDate(), "yyyy/MM/dd"));
		List<Car> carList = carProduction.getCarList();
		int typeACount = 0;
		int typeBCount = 0;
		int typeCCount = 0;
		for (Car car : carList) {
			TYPE type = car.getModelType();
			switch (type) {
			case A:
				typeACount++;
				break;
			case B:
				typeBCount++;
				break;
			case C:
				typeCCount++;
				break;
			default:
				break;
			}
		}
		if (typeACount > 0) {
			carProductionView.getTypeProductionMap().put(TYPE.A.name(), typeACount);
		}
		if (typeBCount > 0) {
			carProductionView.getTypeProductionMap().put(TYPE.B.name(), typeBCount);
		}
		if (typeCCount > 0) {
			carProductionView.getTypeProductionMap().put(TYPE.C.name(), typeCCount);
		}
		carProductionView.setTotalProduction(typeACount + typeBCount + typeCCount);
		return carProductionView;
	}

	private List<CarProductionView> createCarProductionView(List<CarProduction> productionList, boolean ordered) {
		return createCarProductionView(productionList, null, ordered);
	}

	private List<CarProductionView> createCarProductionView(List<CarProduction> productionList, List<Car> cars, boolean ordered) {
		List<CarProductionView> carProductionViewList = new ArrayList<>();
		List<CarProductionView> carProductionViewListTemp = new ArrayList<>();
		for (CarProduction carProduction : productionList) {
			carProductionViewListTemp.add(fullFillCarProductionView(carProduction));
			if (cars != null) {
				cars.addAll(carProduction.getCarList());
			}
		}
		if (!ordered) {
			carProductionViewListTemp = carProductionViewListTemp.stream().sorted(Comparator.comparing(CarProductionView::getDate)).collect(Collectors.toList());
		}
		List<String> allDate = new ArrayList<>();
		for (CarProductionView carProductionView : carProductionViewListTemp) {
			if (!allDate.contains(carProductionView.getDate())) {
				allDate.add(carProductionView.getDate());
			}
		}
		for (String date : allDate) {
			List<CarProductionView> filteredList = carProductionViewListTemp.stream().filter(carProductionView -> date.equals(carProductionView.getDate())).collect(Collectors.toList());
			CarProductionView carProductionViewToAdd = new CarProductionView(filteredList.get(0).getUuid());
			carProductionViewToAdd.setDate(date);
			int totalProduction = 0;
			for (CarProductionView carProductionView : filteredList) {
				String type = null;
				int production = 0;
				Set<Entry<String, Integer>> entrySet = carProductionView.getTypeProductionMap().entrySet();
				for (Entry<String, Integer> entry : entrySet) {
					type = entry.getKey();
					production = entry.getValue();
					carProductionViewToAdd.getTypeProductionMap().put(type, production);
					totalProduction += production;
				}
			}
			carProductionViewToAdd.setTotalProduction(totalProduction);
			reverseDate(carProductionViewToAdd);
			carProductionViewList.add(carProductionViewToAdd);
		}
		return carProductionViewList;
	}

	private void reverseDate(CarProductionView carProductionViewToAdd) {
		String[] dateParsed = carProductionViewToAdd.getDate().split("/");
		String day = dateParsed[2];
		String month = dateParsed[1];
		String year = dateParsed[0];
		String newDate = day + "/" + month + "/" + year;
		carProductionViewToAdd.setDate(newDate);
	}

	private List<CarByTypeView> fullFillCarsTypeView(List<Car> cars) {
		List<CarByTypeView> carsByTypeView = new ArrayList<>();
		for (TYPE type : TYPE.values()) {
			List<Car> typeCars = cars.stream().filter(car -> car.getModelType().equals(type)).collect(Collectors.toList());
			carsByTypeView.add(mapWithCarList(typeCars));
		}
		return carsByTypeView;
	}

	private CarByTypeView mapWithCarList(List<Car> carsByType) {
		CarByTypeView carByTypeView = new CarByTypeView();
		carByTypeView.setModelType(carsByType.get(0).getModelType().name());
		carByTypeView.setSingleCostPrice(carsByType.get(0).getProductionPrice());
		carByTypeView.setSingleSellPrice(carsByType.get(0).getSellPrice());
		carByTypeView.setProduction(carsByType.size());
		carByTypeView.setTotalCostPrice(carByTypeView.getSingleCostPrice().multiply(BigDecimal.valueOf(carsByType.size())));
		carByTypeView.setTotalSellPrice(carByTypeView.getSingleSellPrice().multiply(BigDecimal.valueOf(carsByType.size())));
		return carByTypeView;
	}
}
