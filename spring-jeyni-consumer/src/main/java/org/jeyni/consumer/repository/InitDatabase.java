package org.jeyni.consumer.repository;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.jeyni.model.entity.Car;
import org.jeyni.model.entity.CarProduction;
import org.jeyni.model.entity.TYPE;
import org.jeyni.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitDatabase implements CommandLineRunner {

	@Autowired
	private CarRepository carRepository;

	@Autowired
	private CarProductionRepository carProductionRepository;

	@Override
	public void run(String... strings) throws Exception {

		// initialize CarProduction list
		List<CarProduction> carProductionList = new ArrayList<>();

		// Create Car list:
		int i = 0;
		int day = i + 1;
		createProduction(carProductionList, day, 10, 12, 8);
		for (i = 1; i < 15; i++) {
			day = i + 1;
			int typeAProduction = new Random().nextInt(15) + 10;
			int typeBProduction = new Random().nextInt(5) + 20;
			int typeCProduction = new Random().nextInt(2) + 5;
			createProduction(carProductionList, day, typeAProduction, typeBProduction, typeCProduction);
		}
		this.carProductionRepository.save(carProductionList);

	}

	private void createProduction(List<CarProduction> carProductionList, int day, int typeAProduction, int typeBProduction, int typeCProduction) throws ParseException {
		List<Car> carList = new ArrayList<>();
		String formatter = "%02d";
		StringBuilder stringDate = new StringBuilder(String.format(formatter, day));
		stringDate.append("/01/2018");
		Date date = Helper.convertStringToSQLDate(stringDate.toString(), "dd/MM/yyyy");
		for (int i = 0; i < typeAProduction; i++) {
			carList.add(new Car(3000, 2650.25, TYPE.A));
		}
		for (int i = 0; i < typeBProduction; i++) {
			carList.add(new Car(3250.50, 2750.25, TYPE.B));
		}
		for (int i = 0; i < typeCProduction; i++) {
			carList.add(new Car(4223.35, 2896.25, TYPE.C));
		}
		this.carRepository.save(carList);
		carProductionList.add(new CarProduction(date, carList));
	}
}
