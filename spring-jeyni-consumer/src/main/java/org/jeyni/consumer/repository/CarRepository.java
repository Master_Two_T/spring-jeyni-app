package org.jeyni.consumer.repository;

import org.jeyni.model.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

	Car findByUuid(String uuid);

}
