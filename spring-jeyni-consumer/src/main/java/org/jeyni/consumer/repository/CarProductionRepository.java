package org.jeyni.consumer.repository;

import java.sql.Date;
import java.util.List;

import org.jeyni.model.entity.CarProduction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarProductionRepository extends JpaRepository<CarProduction, Long> {
	
	public CarProduction findByDate(Date date);
	
	public List<CarProduction> findAllByOrderByDateAsc();
	
	public List<CarProduction> findByDateBetweenOrderByDateAsc(Date start, Date stop);

}